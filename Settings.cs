﻿using System;
using System.Configuration;

namespace $safeprojectname$
{
    public class Settings
    {
        public static string ApiUrl
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["ApiUrl"]; }
        }

        public static string Username
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["Username"]; }
        }

        public static string Password
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["Password"]; }
        }

        public static string Path
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["Path"]; }
        }
    }
}
