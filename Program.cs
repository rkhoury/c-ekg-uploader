﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.Xml;
using System.Security.Cryptography;
using System.Web;
using System.Net;
using System.Drawing;
using System.Collections.Specialized;
using Newtonsoft.Json;

namespace $safeprojectname$
{

    class Login
    {
        public string u { get; set; }
        public string p { get; set; }

    }

    public class Phone
    {

        public string phone { get; set; }

        public string type { get; set; }

        public string ext { get; set; }

    }

    public class Patient
    {

        public int id { get; set; }

        [JsonProperty("first_name")]
        public string firstname { get; set; }

        [JsonProperty("last_name")]
        public string lastname { get; set; }

        public string dob { get; set; }

        public Phone phone { get; set; }

        public Phone phone2 { get; set; }

        public Phone phone3 { get; set; }

        public string ramq { get; set; }

        public string ramqexpiryyear { get; set; }

        public string ramqexpirymonth { get; set; }
    }

    public class FileType
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("description")]
        public string description { get; set; }
        [JsonProperty("idtypeanalyse")]
        public string idtypeanalyse { get; set; }
    }

    public class PatientContainer
    {
        public List<Patient> patients { get; set; }
    }

    public class PatientException
    {
        public static string noFirstName() { return "There is no first name entered in the XML file for patient: "; }
        public static string noLastName() { return "There is no last name entered in the XML file for patient: "; }
        public static string noDateOfBirth() { return "There is no date of birth entered in the XML file for patient: "; }
        public static string noPatientExists() { return "There is no entry in the Medesync database for patient: "; }
    }


    class Program
    {
        private readonly static string UPLOADFILETYPE = "autres";

        public static WebClient wc;
        public static string cookie;
        public static string filetype = null;

        [STAThread]
        static void Main(string[] args)
        {

            LoginPopup();
            // Finds the location that contains the XML and PDF files
            string path = "";
            string processedFolder = "";
            string failedFolder = "";
            string debugFolder = "";
            string failedTextFile = "errors.txt";
            string processedTextFile = "completed.txt";
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {

                path = dialog.SelectedPath;

                // Creates the history folder
                processedFolder = System.IO.Path.Combine(path, "Processed");
                if (!Directory.Exists(processedFolder)) System.IO.Directory.CreateDirectory(processedFolder);

                //Cannot process folder
                failedFolder = System.IO.Path.Combine(path, "Failed");
                if (!Directory.Exists(failedFolder)) System.IO.Directory.CreateDirectory(failedFolder);

                //Dev files folder
                debugFolder = System.IO.Path.Combine(path, "Debug");
                if (!Directory.Exists(debugFolder)) System.IO.Directory.CreateDirectory(debugFolder);

                // Creates the errors txt
                failedTextFile = System.IO.Path.Combine(debugFolder, failedTextFile);
                if (!File.Exists(failedTextFile))
                {
                    File.Create(failedTextFile);
                }

                // Creates the completed txt
                processedTextFile = System.IO.Path.Combine(debugFolder, processedTextFile);
                if (!File.Exists(processedTextFile))
                {
                    File.Create(processedTextFile);
                }

            }

            // Sets the available file types and finds the other file type
            List<FileType> ftc = JsonConvert.DeserializeObject<List<FileType>>(wc.DownloadString("https://dev.medesync.com/api/v2/patient/get_filetype.ashx"));

            foreach (FileType f in ftc)
            {
                if (String.Equals(f.description, UPLOADFILETYPE, StringComparison.OrdinalIgnoreCase))
                {
                    filetype = f.id;
                    break;
                }

            }

            // If it didn't find the other file type
            if (String.IsNullOrWhiteSpace(filetype))
            {
                foreach (FileType f in ftc)
                {
                    // If it got no information
                    if (String.IsNullOrWhiteSpace(f.idtypeanalyse))
                    {
                        filetype = f.id;
                        break;
                    }

                    // Otherwise it set the string as "null"
                    if (String.Equals(f.idtypeanalyse, "null", StringComparison.OrdinalIgnoreCase))
                    {
                        filetype = f.id;
                        break;
                    }
                }
            }

            // Exits after too many loops
            int loops = 0;
            int maxLoops = 100;

            // Continues until exit
            while (true)
            {
                Debug.WriteLine(loops);
                if (loops > maxLoops)
                {
                    Form prompt2 = new Form();
                    prompt2.Icon = Properties.Resources.Medesync;
                    prompt2.Width = 500;
                    prompt2.Height = 250;
                    prompt2.FormBorderStyle = FormBorderStyle.FixedDialog;
                    prompt2.Text = "Complete";
                    prompt2.StartPosition = FormStartPosition.CenterScreen;
                    Label triesExceeded = new Label() { Left = 50, Top = 20, Text = "Exiting." };
                    prompt2.Controls.Add(triesExceeded);
                    prompt2.ShowDialog();
                    Environment.Exit(0);
                }
                loops++;
                // Stores the path of the XML files in a string array 
                string[] files = System.IO.Directory.GetFiles(path, "*.xml");

                // Puts the program to sleep while there are no XML files in the directory specified
                if (files.Length == 0)
                {
                    Console.WriteLine("No files found. Sleeping for 5 seconds! ZzZzZ");
                    Console.WriteLine("Program will shut down after " + (maxLoops - loops + 1) + " more sleeps" + "\n");
                    Thread.Sleep(5000);
                    continue;
                }

                // If there are XML files located in this folder, finds them and extracts the information necessary
                if (files.Length > 0)
                {
                    foreach (string s in files)
                    {

                        int id = -1;

                        // Finds the corresponding PDF located in the same folder
                        string pdf = String.Copy(s);
                        string extension = Path.GetExtension(pdf);
                        pdf = Path.ChangeExtension(pdf, ".pdf");

                        // If a matching PDF file exists
                        if (!File.Exists(pdf))
                        {
                            break;
                        }
                        WaitForFile(pdf);

                        XmlDocument doc = new XmlDocument(); // Create an XML document object
                        doc.Load(s); // Load the XML document from the specified file

                        // Get elements
                        XmlNodeList patientFirstName = doc.GetElementsByTagName("patient_first_name");
                        XmlNodeList patientLastName = doc.GetElementsByTagName("patient_last_name");
                        XmlNodeList patientDOB = doc.GetElementsByTagName("patient_birth_date");
                        WaitForFile(s);

                        // If Patient first name does not exist
                        if (String.IsNullOrWhiteSpace(patientFirstName[0].InnerText))
                        {
                            WaitForFile(failedTextFile);
                            Console.WriteLine("FAILURE: " + PatientException.noFirstName() + Path.GetFileNameWithoutExtension(s) + "\n");
                            writeToFile(failedTextFile, PatientException.noFirstName() + Path.GetFileNameWithoutExtension(s) + "\n");
                            WaitForFile(failedTextFile);
                            WaitForFile(s);
                            WaitForFile(pdf);
                            moveToFolder(s, pdf, failedFolder);
                            break;
                        }

                        // If Patient last name does not exist
                        if (String.IsNullOrWhiteSpace(patientLastName[0].InnerText))
                        {
                            WaitForFile(failedTextFile);
                            Console.WriteLine("FAILURE: " + PatientException.noLastName() + Path.GetFileNameWithoutExtension(s) + "\n");
                            writeToFile(failedTextFile, PatientException.noLastName() + Path.GetFileNameWithoutExtension(s) + "\n");
                            WaitForFile(failedTextFile);
                            WaitForFile(s);
                            WaitForFile(pdf);
                            moveToFolder(s, pdf, failedFolder);
                            break;
                        }

                        // If Patient date of birth does not exist
                        if (String.IsNullOrWhiteSpace(patientDOB[0].InnerText))
                        {
                            WaitForFile(failedTextFile);
                            Console.WriteLine("FAILURE: " + PatientException.noDateOfBirth() + Path.GetFileNameWithoutExtension(s) + "\n");
                            writeToFile(failedTextFile, PatientException.noDateOfBirth() + Path.GetFileNameWithoutExtension(s) + "\n");
                            WaitForFile(failedTextFile);
                            WaitForFile(s);
                            WaitForFile(pdf);
                            moveToFolder(s, pdf, failedFolder);
                            break;
                        }

                        // Downloads the patient information for all patients with this first name
                        PatientContainer patientJSON = JsonConvert.DeserializeObject<PatientContainer>(wc.DownloadString("https://dev.medesync.com/api/v2/patient/list.ashx?filter=" + patientFirstName[0].InnerText + "&offset=1"));

                        // Checks for each of the patients it has downloaded
                        foreach (Patient p in patientJSON.patients)
                        {
                            if (String.Equals(p.firstname, (string)patientFirstName[0].InnerText, StringComparison.OrdinalIgnoreCase) &&
                                (String.Equals(p.lastname, (string)patientLastName[0].InnerText, StringComparison.OrdinalIgnoreCase)) &&
                                (String.Equals(p.dob, (string)patientDOB[0].InnerText, StringComparison.OrdinalIgnoreCase)))

                            // Patient found, sets id
                            {
                                id = p.id;
                                break;
                            }

                        }
                        // No patients found
                        if (id == -1)
                        {
                            WaitForFile(failedTextFile);
                            Console.WriteLine("FAILURE: " + PatientException.noPatientExists() + Path.GetFileNameWithoutExtension(s) + "\n");
                            writeToFile(failedTextFile, PatientException.noPatientExists() + Path.GetFileNameWithoutExtension(s) + "\n");
                            WaitForFile(failedTextFile);
                            WaitForFile(s);
                            WaitForFile(pdf);
                            moveToFolder(s, pdf, failedFolder);
                            break;
                        }

                        // Uploads the file
                        try
                        {

                            using (wc)
                            {
                                // Tries to upload
                                Uri uri = new Uri("https://dev.medesync.com/api/v2/patient/save_attachedFile.ashx?IdPatient=" + id + "&FileName=" + Path.GetFileName(pdf) + "&IdFileType=" + filetype);
                                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                                byte[] responseArray = wc.UploadFile(uri, pdf);
                            }
                        }
                        catch (Exception e)
                        {
                            // Could not upload
                            WaitForFile(failedTextFile);
                            Console.WriteLine("FAILURE: " + Path.GetFileNameWithoutExtension(s) + " Error:" + e.ToString() + "\n");
                            writeToFile(failedTextFile, Path.GetFileNameWithoutExtension(s) + " Error:" + e.ToString() + "\n");
                            WaitForFile(failedTextFile);
                            WaitForFile(s);
                            WaitForFile(pdf);
                            moveToFolder(s, pdf, failedFolder);
                            break;
                        }

                        // Moves files
                        WaitForFile(processedTextFile);
                        Console.WriteLine("SUCCESS! Patient Complete: " + Path.GetFileNameWithoutExtension(s) + "\n");
                        writeToFile(processedTextFile, "Patient Complete: " + Path.GetFileNameWithoutExtension(s) + "\n");
                        WaitForFile(failedTextFile);
                        WaitForFile(s);
                        WaitForFile(pdf);
                        moveToFolder(s, pdf, processedFolder);
                    }
                }
                // Puts the thread to sleep
                Console.WriteLine("No files found. Sleeping for 5 seconds! ZzZzZ");
                Console.WriteLine("Program will shut down after " + (maxLoops - loops + 1) + " more sleeps" + "\n");
                Thread.Sleep(5000);
            }

        }

        // Writes text to files without errors
        public static void writeToFile(string file, string text)
        {
            int maxRetry = 5;
            for (int retry = 0; retry < maxRetry; retry++)
            {
                try
                {
                    using (StreamWriter str = new StreamWriter(file, true))
                    {
                        str.WriteLine(text);
                        str.Flush();
                        str.Close();
                        break;
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.ToString());
                }
            }
        }

        // Waits for the file
        public static void WaitForFile(string file)
        {
            // Sleeps
            while (FileInUse(file)) ;
        }

        // Makes sure the file is not in use (thread compatibility)
        public static bool FileInUse(string file)
        {
            // Checks to see if the file exists
            try
            {
                if (!System.IO.File.Exists(file)) 
                {
                    return false;
                }

                // Otherwise make sure the file is not open
                using (System.IO.FileStream stream = new System.IO.FileStream(file, System.IO.FileMode.Open))
                {
                    return false;
                }
            }
            catch
            {
                return true;
            }
        }

        static void moveToFolder(string xml, string pdf, string folder)
        {
            //Moves both files to the History folder
            string pdfFileName = Path.GetFileName(pdf);
            string xmlFileName = Path.GetFileName(xml);
            File.Move(pdf, folder + "\\" + pdfFileName);
            File.Move(xml, folder + "\\" + xmlFileName);
        }

        static void LoginPopup()
        {
            // Authenticates the user
            bool complete = false;
            int tries = 0;
            while (!complete)
            {
                // Too many tries
                if (tries > 3)
                {
                    Form prompt2 = new Form();
                    prompt2.Icon = Properties.Resources.Medesync;
                    prompt2.Width = 500;
                    prompt2.Height = 250;
                    prompt2.FormBorderStyle = FormBorderStyle.FixedDialog;
                    prompt2.Text = "TRIES EXCEEDED";
                    prompt2.StartPosition = FormStartPosition.CenterScreen;
                    Label triesExceeded = new Label() { Left = 50, Top = 20, Text = "Too many login tries." };
                    prompt2.Controls.Add(triesExceeded);
                    prompt2.ShowDialog();
                    Thread.Sleep(5000);
                    Application.Exit();
                }
                // Creates the Login Popup
                Form prompt = new Form();
                prompt.Icon = Properties.Resources.Medesync;
                prompt.Width = 500;
                prompt.Height = 250;
                prompt.FormBorderStyle = FormBorderStyle.FixedDialog;
                prompt.Text = "EKG Uploader";
                prompt.StartPosition = FormStartPosition.CenterScreen;
                Label usernameLabel = new Label() { Left = 50, Top = 20, Text = "Username" };
                TextBox usernameTextBox = new TextBox() { Left = 50, Top = 45, Width = 400 };
                Label passwordLabel = new Label() { Left = 50, Top = 80, Text = "Password" };
                TextBox passwordTextBox = new TextBox() { Left = 50, Top = 105, Width = 400 };
                passwordTextBox.PasswordChar = '*';
                Button confirmation = new Button() { Text = "Ok", Left = 350, Width = 100, Top = 150 };
                confirmation.Click += (sender, e) => { prompt.Close(); };
                prompt.Controls.Add(usernameTextBox);
                prompt.Controls.Add(confirmation);
                prompt.Controls.Add(usernameLabel);
                prompt.Controls.Add(passwordLabel);
                prompt.Controls.Add(passwordTextBox);
                prompt.AcceptButton = confirmation;
                prompt.ShowDialog();
                tries++;
                complete = (AuthenticateUser(usernameTextBox.Text, passwordTextBox.Text));
            }

        }

        static bool AuthenticateUser(string u, string p)
        {
            // Authentication fails if either of the fields are empty
            if (u == null || p == null)
            {
                return false;
            }

            // Hashes the password field
            MD5 md5Hash = MD5.Create();
            byte[] hashedPassword = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(p));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < hashedPassword.Length; i++)
            {
                sBuilder.Append(hashedPassword[i].ToString("x2"));
                Debug.WriteLine(sBuilder.ToString());
            }


            // Authenticates user
            try
            {
                using (wc = new WebClient())
                {

                    // Logs in the particular user
                    string URLAuth = "https://dev.medesync.com/api/v2/user/login.ashx";
                    NameValueCollection formData = new NameValueCollection();
                    formData["u"] = u;
                    formData["p"] = sBuilder.ToString();

                    // Stores the login in a cookie
                    byte[] responseBytes = wc.UploadValues(URLAuth, "POST", formData);
                    cookie = wc.ResponseHeaders["Set-Cookie"].ToString();
                    wc.Headers.Add("Cookie", cookie);
                }
            }

            //
            catch (Exception e)
            {
                return false;
            }

            return true;
        }
    }
}
